const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

// var id = '5aa0b66a46342024d0a41e1c+++';
//
// if (!ObjectID.isValid(id)) {
//   console.log('ID not valid');
// }

// Todo.find({
//   _id: id
// }).then((todos) => {
//   console.log(`Todos: ${todos}`);
// });
//
// Todo.findOne({
//   _id: id
// }).then((todo) => {
//   console.log(`Todos: ${todo}`);
// });

// Todo.findById(id)
//   .then((todo) => {
//     if (!todo) {
//       return console.log('Id not found.');
//     }
//     console.log(`Todo By Id: ${todo}`);
//   })
//   .catch((e) => console.log(e));

var userId = '5a9f46be938b2108d4a00563';

User.findById(userId)
  .then((user) => {
    if (!user) {
      return console.log('User ID not found.');
    }
    console.log(`User By Id: ${user}`);
  })
  .catch((e) => console.log(e));
