const expect = require('expect');
const request = require('supertest');
const {ObjectID} = require('mongodb');

const {app} = require('./../server');
const {Todo} = require('./../models/todo');
const {User} = require('./../models/user');
const {todos, populateTodos, users, populateUsers} = require('./seed/seed');

beforeEach(populateUsers);
beforeEach(populateTodos);

describe('POST /todos', () => {
  it('should create a new todo', (done) => {
    var text = 'Test todo text';
    var token = users[0].tokens[0].token;

    request(app)
      .post('/todos')
      .set('x-auth', token)
      .send({
        text
      })
      .expect(200)
      .expect((response) => {
        expect(response.body.text).toBe(text);
      })
      .end((err, response) => {
        if (err) {
          return done(err);
        }

        Todo.find({text}).then((todos) => {
          expect(todos).toHaveLength(1);
          expect(todos[0].text).toBe(text);
          done();
        }).catch((e) => done(e));
      });
  });

  it('should not create todo with invalid body data', (done) => {
    var token = users[0].tokens[0].token;

    request(app)
      .post('/todos')
      .set('x-auth', token)
      .send({})
      .expect(400)
      .end((err, response) => {
        if (err) {
          return done(err);
        }

        Todo.find().then((todos) => {
          expect(todos).toHaveLength(2);
          done();
        }).catch((e) => done(e));
      });
  });
});

describe('GET /todos', () => {
  it('should get all todos', (done) => {
    var token = users[0].tokens[0].token;

    request(app)
      .get('/todos')
      .set('x-auth', token)
      .expect(200)
      .expect((response) => {
        expect(response.body.todos).toHaveLength(1);
      })
      .end(done);
  });
});

describe('GET /todos/:id', () => {
  it('should return todo doc', (done) => {
    var token = users[0].tokens[0].token;

    request(app)
      .get(`/todos/${todos[0]._id.toHexString()}`)
      .set('x-auth', token)
      .expect(200)
      .expect((response) => {
        expect(response.body.todo.text).toBe(todos[0].text);
      })
      .end(done);
  });

  it('should not return todo doc created by other user', (done) => {
    var token = users[0].tokens[0].token;

    request(app)
      .get(`/todos/${todos[1]._id.toHexString()}`)
      .set('x-auth', token)
      .expect(404)
      .end(done);
  });

  it('should return 404 if todo not found', (done) => {
    var token = users[0].tokens[0].token;

    request(app)
      .get(`/todos/${new ObjectID().toHexString()}`)
      .set('x-auth', token)
      .expect(404)
      .end(done);
  });

  it('should return 404 for non-object ids', (done) => {
    var token = users[0].tokens[0].token;

    request(app)
      .get('/todos/123')
      .set('x-auth', token)
      .expect(404)
      .end(done);
  });
});

describe('DELETE /todos/:id', () => {
  it('should remove a todo', (done) => {
    var id = todos[1]._id.toHexString();
    var token = users[1].tokens[0].token;

    request(app)
      .delete(`/todos/${id}`)
      .set('x-auth', token)
      .expect(200)
      .expect((response) => {
        expect(response.body.todo._id).toBe(id);
      })
      .end((err, response) => {
        if (err) {
          return done(err);
        }

        Todo.findById(id).then((todo) => {
          expect(todo).toBeNull();
          done();
        }).catch((e) => done(e));
      })
  });

  it('should not remove a todo', (done) => {
    var id = todos[0]._id.toHexString();
    var token = users[1].tokens[0].token;

    request(app)
      .delete(`/todos/${id}`)
      .set('x-auth', token)
      .expect(404)
      .end((err, response) => {
        if (err) {
          return done(err);
        }

        Todo.findById(id).then((todo) => {
          expect(todo).toBeDefined();
          done();
        }).catch((e) => done(e));
      })
  });

  it('should return 404 if todo not found', (done) => {
    var token = users[1].tokens[0].token;

    request(app)
      .get(`/todos/${new ObjectID().toHexString()}`)
      .set('x-auth', token)
      .expect(404)
      .end(done);
  });

  it('should return 404 if object id is invalid', (done) => {
    var token = users[1].tokens[0].token;

    request(app)
      .get('/todos/123')
      .set('x-auth', token)
      .expect(404)
      .end(done);
  });
});

describe('PATCH /todos/:id', () => {
  it('should update the todo', (done) => {
    var id = todos[0]._id.toHexString();
    var text = 'Unit test patch todo';
    var completed = true;
    var token = users[0].tokens[0].token;

    request(app)
      .patch(`/todos/${id}`)
      .send({text, completed})
      .set('x-auth', token)
      .expect(200)
      .expect((response) => {
        expect(response.body.todo.text).toBe(text);
        expect(response.body.todo.completed).toBeTruthy();
        expect(typeof response.body.todo.completedAt).toBe('number');
      })
      .end(done);
  });

  it('should not update the todo of another user', (done) => {
    var id = todos[0]._id.toHexString();
    var text = 'Unit test patch todo';
    var completed = true;
    var token = users[1].tokens[0].token;

    request(app)
      .patch(`/todos/${id}`)
      .send({text, completed})
      .set('x-auth', token)
      .expect(404)
      .end(done);
  });

  it('should clear completedAt when todo is not completed', (done) => {
    var id = todos[1]._id.toHexString();
    var text = 'Unit test set this completedAt to null';
    var completed = false;
    var token = users[1].tokens[0].token;

    request(app)
      .patch(`/todos/${id}`)
      .send({text, completed})
      .set('x-auth', token)
      .expect(200)
      .expect((response) => {
        expect(response.body.todo.text).toBe(text);
        expect(response.body.todo.completed).toBeFalsy();
        expect(response.body.todo.completedAt).toBeNull();
      })
      .end(done);
  });

  it('should return 404 if id does not exist', (done) => {
    var text = 'This text should not be used';
    var completed = false;
    var token = users[0].tokens[0].token;

    request(app)
      .patch(`/todos/${new ObjectID().toHexString()}`)
      .set('x-auth', token)
      .send({text, completed})
      .expect(404)
      .end(done);
  });

  it('should return 404 if object id is invalid', (done) => {
    var text = 'This text should not be used';
    var completed = false;
    var token = users[0].tokens[0].token;

    request(app)
      .patch('/todos/123')
      .set('x-auth', token)
      .send({text, completed})
      .expect(404)
      .end(done);
  });
});

describe('GET /users/me', () => {
  it('should return user if authenticated', (done) => {
    request(app)
      .get('/users/me')
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .expect((response) => {
        expect(response.body._id).toBe(users[0]._id.toHexString());
        expect(response.body.email).toBe(users[0].email);
      })
      .end(done);
  });

  it('should return a 401 if not authenticated', (done) => {
    request(app)
      .get('/users/me')
      .expect(401)
      .expect((response) => {
        expect(response.body).toEqual({});
      })
      .end(done);
  });
});

describe('POST /users', () => {
  it('should create a user', (done) => {
    var email = 'example@example.com';
    var password = '123mnb!';

    request(app)
      .post('/users')
      .send({email, password})
      .expect(200)
      .expect((response) => {
        expect(response.headers['x-auth']).toBeDefined();
        expect(response.body._id).toBeDefined();
        expect(response.body.email).toBe(email);
      })
      .end((err) => {
        if (err) {
          return done(err);
        }

        User.findOne({email}).then((user) => {
          expect(user).toBeDefined();
          expect(user.password).not.toBe(password);
          done();
        }).catch((e) => done(e));
      });
  });

  it('should return validation errors if request invalid', (done) => {
    var email = 'invalid@email';
    var password = 'bad';

    request(app)
      .post('/users')
      .send({email, password})
      .expect(400)
      .end(done);
  });

  it('should not create user if email in use', (done) => {
    var email = users[0].email;
    var password = 'superSecureGAINSVILLZOMG_3214234234324';

    request(app)
      .post('/users')
      .send({email, password})
      .expect(400)
      .end(done);
  });
});

describe('POST /users/login', () => {
  it('should login user and return auth token', (done) => {
    var {email, password} = users[1];

    request(app)
      .post('/users/login')
      .send({email, password})
      .expect(200)
      .expect((response) => {
        expect(response.headers['x-auth']).toBeDefined();
      })
      .end((err, response) => {
        if (err) {
          return done(err);
        }

        User.findById(users[1]._id).then((user) => {
          expect(user.tokens[1]).toEqual(
            expect.objectContaining({
              access: 'auth',
              token: response.headers['x-auth']
            })
          );
          done();
        }).catch((e) => done(e));;
      });
  });

  it('should reject invalid login', (done) => {
    var {email} = users[1];
    var password = 'IncorrectPassword';

    request(app)
      .post('/users/login')
      .send({email, password})
      .expect(400)
      .expect((response) => {
        expect(response.headers['x-auth']).not.toBeDefined();
      })
      .end((err, response) => {
        if (err) {
          return done(err);
        }

        User.findById(users[1]._id).then((user) => {
          expect(user.tokens).toHaveLength(1);
          done();
        }).catch((e) => done(e));;
      });
  });
});

describe('DELETE /users/me/token', () => {
  it('should remove auth token on logout', (done) => {
    var {token} = users[0].tokens[0];

    request(app)
      .delete('/users/me/token')
      .set('x-auth', token)
      .expect(200)
      .end((err, response) => {
        if (err) {
          return done(err);
        }

        User.findById(users[0]._id).then((user) => {
          expect(user.tokens).toHaveLength(0);
          done();
        }).catch((e) => done(e));;
      });
  });
});
